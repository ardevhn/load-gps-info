using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using LoadGpsInfo.Domain.Entities;

namespace LoadGpsInfo.Data
{
    public class UserLoginSessionAutoMappingOverride : IAutoMappingOverride<UserLoginSession>
    {
        public void Override(AutoMapping<UserLoginSession> mapping)
        {            
        }
    }
}