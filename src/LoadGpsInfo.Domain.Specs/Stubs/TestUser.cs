﻿using System;
using LoadGpsInfo.Domain.Entities;
using LoadGpsInfo.Domain.ValueObjects;

namespace LoadGpsInfo.Domain.Specs.Stubs
{
    public class TestUser : UserEmailLogin
    {
        public TestUser(Guid userId, string name, string password)
        {
            Id = userId;
            Name = name;
            this.EncryptedPassword = password;
        }
    }
}