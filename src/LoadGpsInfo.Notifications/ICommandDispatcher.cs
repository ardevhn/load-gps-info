using Autofac.Extras.DynamicProxy2;

namespace LoadGpsInfo.Notifications
{
   
    public interface ICommandDispatcher
    {
        void Dispatch(IUserSession userSession, object command);
    }
}