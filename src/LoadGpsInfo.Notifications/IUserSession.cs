using System;

namespace LoadGpsInfo.Notifications
{
    public interface IUserSession
    {
        Guid Id { get; }
    }
}