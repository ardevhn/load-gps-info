﻿angular.module('LoadGpsInfo.Services').factory('gpsService', ['$http', '$q', 
    function ($http, $q) {

    return {        
        GetGpsList: function() {
            var defer = $q.defer();

            $http.get("/gps/list")
                .success(function (data) {
                    defer.resolve(data);
                }).error(function (data) {
                    defer.reject(data);
                });

            return defer.promise;
        }
    };
}]);