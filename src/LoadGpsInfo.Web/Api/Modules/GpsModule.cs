﻿using System;
using System.Collections.Generic;
using System.Linq;
using AcklenAvenue.Commands;
using AutoMapper;
using LoadGpsInfo.Domain.Application.Commands;
using LoadGpsInfo.Domain.Entities;
using LoadGpsInfo.Domain.Services;
using LoadGpsInfo.Web.Api.Requests.GPS;
using LoadGpsInfo.Web.Api.Responses.GPS;
using Nancy;
using Nancy.ModelBinding;
using LoadGpsInfo.Web.Api.Infrastructure;
using LoadGpsInfo.Web.Api.Requests;
using NHibernate.Linq;

namespace LoadGpsInfo.Web.Api.Modules
{
    public class GpsModule : NancyModule
    {
        public GpsModule(IReadOnlyRepository readOnlyRepository,
            ICommandDispatcher commandDispatcher,
            IPasswordEncryptor passwordEncryptor,
            IMappingEngine mappingEngine,
            IWriteableRepository writeableRepository)
        {
            Post["/gps/register"] = _ =>
            {
                var req = this.Bind<GpsInfoRequest>();

                commandDispatcher.Dispatch(this.UserSession(),
                    new GpsInfoAdd(Guid.NewGuid(), req.Name, req.Description, req.Latitud, req.Longitud,
                        req.Gallons, req.Temperature, req.Date));

                return null;
            };

            Get["/gps/list"] = _ =>
            {
                var gpsList = readOnlyRepository.GetAll<GpsInfo>();

                var result = mappingEngine.Map<IEnumerable<GpsInfo>,
                    IEnumerable<GpsInfoResponse>>(gpsList);

                return result;
            };

            Get["/gps/query"] = _ =>
            {
                var resultList = new List<GpsInfo>();
                var gpsList =
                            readOnlyRepository.Query<GpsInfo>(gps => gps.QueriedDate == null);

                gpsList.ForEach(gps =>
                                {
                                    gps.UpdateQueryDate();
                                    resultList.Add(gps);
                                    writeableRepository.Update(gps);
                                });

                var result = mappingEngine.Map<IEnumerable<GpsInfo>,
                    IEnumerable<GpsInfoResponse>>(resultList);

                return result;
            };
        }
    }
}