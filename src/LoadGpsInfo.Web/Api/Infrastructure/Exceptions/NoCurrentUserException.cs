using System;

namespace LoadGpsInfo.Web.Api.Infrastructure.Exceptions
{
    public class NoCurrentUserException : Exception
    {
    }
}