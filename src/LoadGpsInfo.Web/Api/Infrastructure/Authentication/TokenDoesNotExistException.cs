using System;

namespace LoadGpsInfo.Web.Api.Infrastructure.Authentication
{
    public class TokenDoesNotExistException : Exception
    {
    }
}