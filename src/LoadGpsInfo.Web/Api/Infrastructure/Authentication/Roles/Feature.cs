﻿using System;
using System.Linq;
using System.Web;
using AutoMapper.Internal;
using LoadGpsInfo.Web.Api.Requests.Google;

namespace LoadGpsInfo.Web.Api.Infrastructure.Authentication.Roles
{
    public class Feature
    {
         public string Description { get; set; }
    }
}