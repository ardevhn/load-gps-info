using System;

namespace LoadGpsInfo.Web.Api.Infrastructure.RestExceptions
{
    public class TokenDoesNotExistException : Exception
    {
    }
}