using AcklenAvenue.Commands;
using Nancy;
using LoadGpsInfo.Domain.Entities;
using LoadGpsInfo.Domain.Services;

using LoadGpsInfo.Web.Api.Infrastructure.Authentication;
using LoadGpsInfo.Web.Api.Infrastructure.Exceptions;
using LoadGpsInfo.Web.Api.Modules;

namespace LoadGpsInfo.Web.Api.Infrastructure
{
    public static class IdentityExtentions
    {
        public static IUserSession UserSession(this NancyModule module)
        {
            var user = module.Context.CurrentUser as LoggedInUserIdentity;
            if (user == null) throw new NoCurrentUserException();
            return user.UserSession;
        }

        public static UserLoginSession UserLoginSession(this NancyModule module)
        {
            var user = module.Context.CurrentUser as LoggedInUserIdentity;
            if (user == null || user.UserSession.GetType() != typeof(UserLoginSession)) throw new NoCurrentUserException();
            return (UserLoginSession) user.UserSession;
        }
    }
}