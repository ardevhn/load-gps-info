using System;
using AutoMapper;
using Autofac;
using LoadGpsInfo.Domain.Entities;
using LoadGpsInfo.Web.Api.Requests;
using LoadGpsInfo.Web.Api.Responses.Admin;
using LoadGpsInfo.Web.Api.Responses.GPS;

namespace LoadGpsInfo.Web.Api.Infrastructure.Configuration
{
    public class ConfigureAutomapperMappings : IBootstrapperTask<ContainerBuilder>
    {
        #region IBootstrapperTask<ContainerBuilder> Members

        public Action<ContainerBuilder> Task
        {
            get
            {
                return container =>
                    {
                        Mapper.CreateMap<User, AdminUserResponse>();
                        Mapper.CreateMap<UserAbility, UserAbilityRequest>().ReverseMap();
                        Mapper.CreateMap<GpsInfo, GpsInfoResponse>().ReverseMap();
                    };
            }
        }

        #endregion
    }
}