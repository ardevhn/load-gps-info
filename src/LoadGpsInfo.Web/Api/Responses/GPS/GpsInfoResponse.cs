﻿using System;

namespace LoadGpsInfo.Web.Api.Responses.GPS
{
    public class GpsInfoResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? QueriedDate { get; set; }
    }
}