﻿namespace LoadGpsInfo.Web.Api
{
    public interface IBaseUrlProvider
    {
        string GetBaseUrl();
    }
}