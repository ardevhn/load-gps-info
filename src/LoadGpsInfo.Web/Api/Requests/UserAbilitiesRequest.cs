using System;
using System.Collections.Generic;
using LoadGpsInfo.Web.Api.Modules;

namespace LoadGpsInfo.Web.Api.Requests
{
    public class UserAbilitiesRequest
    {
        public IEnumerable<UserAbilityRequest> Abilities { get; set; }
        public Guid UserId { get; set; }
    }
}