namespace LoadGpsInfo.Web.Api.Requests
{
    public class ResetPasswordRequest
    {
        public string Email { get; set; }
    }
}