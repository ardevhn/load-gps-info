﻿using System;
using LoadGpsInfo.Domain.Entities;

namespace LoadGpsInfo.Web.Api.Requests.GPS
{
    public class GpsInfoRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float? Latitud { get; set; }
        public float? Longitud { get; set; }
        public float? Speed { get; set; }
        public float? Gallons { get; set; }
        public float? Temperature { get; set; }
        public DateTime? Date { get; set; }
    }
}