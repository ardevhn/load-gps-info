using System;
using AcklenAvenue.Commands;
using LoadGpsInfo.Domain.Services;


namespace LoadGpsInfo.Domain
{
    public class VisitorSession : IUserSession
    {
        #region IUserSession Members

        public Guid Id { get; private set; }

        #endregion
    }
}