﻿using System;

namespace LoadGpsInfo.Domain.Entities
{
    public class GpsPosition : Entity
    {
        protected GpsPosition()
        {

        }

        public GpsPosition(Guid id, int latitude, int longitude, int altitude)
        {
            this.Id = id;
            this.Latitude = latitude;
            this.Altitude = altitude;
            this.Longitude = longitude;
        }

        public virtual Guid Id { get; protected set; }

        public virtual int Latitude { get; set; }
        public virtual int Longitude { get; set; }
        public virtual int Altitude { get; set; }
    }
}