﻿using System;
using System.Collections.Generic;

namespace LoadGpsInfo.Domain.Entities
{
    public class GpsInfo : Entity
    {
        protected GpsInfo()
        {

        }

        public GpsInfo(Guid id, string name, string description)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.CreationDate = DateTime.Now;
            this.QueriedDate = null;
            this.Date = null;
            this.Latitud = null;
            this.Longitud = null;
            this.Speed = null;
            this.Gallons = null;
            this.Temperature = null;
        }

        public virtual Guid Id { get; protected set; }
        public virtual string Name { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual float? Latitud { get; set; }
        public virtual float? Longitud { get; set; }
        public virtual float? Speed { get; set; }
        public virtual float? Gallons { get; set; }
        public virtual float? Temperature { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime CreationDate { get; protected set; }
        public virtual DateTime? QueriedDate { get; protected set; }

        public virtual void UpdateQueryDate()
        {
            QueriedDate = DateTime.Now;
        }
    }
}
