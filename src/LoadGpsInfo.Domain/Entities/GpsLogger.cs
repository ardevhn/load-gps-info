﻿using System;

namespace LoadGpsInfo.Domain.Entities
{
    public class GpsLogger: Entity
    {
        public GpsLogger()
        {

        }

        public GpsLogger(DateTime insertDate, User insertUser)
        {
            this.InsertDate = insertDate;
            this.InsertUser = insertUser;
        }

        public virtual Guid Id { get; protected set; }

        public virtual DateTime InsertDate { get; protected set; }
        public virtual User InsertUser { get; protected set; }
    }
}
