using System;

namespace LoadGpsInfo.Domain
{
    public interface ITimeProvider
    {
        DateTime Now();
    }
}