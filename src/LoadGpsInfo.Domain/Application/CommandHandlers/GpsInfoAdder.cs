﻿using System;
using System.Diagnostics;
using LoadGpsInfo.Domain.Application.Commands;
using LoadGpsInfo.Domain.DomainEvents;
using LoadGpsInfo.Domain.Entities;
using LoadGpsInfo.Domain.Services;
using AcklenAvenue.Commands;

namespace LoadGpsInfo.Domain.Application.CommandHandlers
{
    public class GpsInfoAdder : ICommandHandler<GpsInfoAdd>
    {
        readonly IWriteableRepository _writeableRepository;
        readonly IReadOnlyRepository _readOnlyRepository;

        public GpsInfoAdder(IWriteableRepository writeableRepository, 
            IReadOnlyRepository readOnlyRepository)
        {
            _writeableRepository = writeableRepository;
            _readOnlyRepository = readOnlyRepository;
        }

        public void Handle(IUserSession userIssuingCommand, GpsInfoAdd command)
        {
            GpsInfo gpsInfo = new GpsInfo(Guid.NewGuid(), command.Name, command.Description)
            {
                Latitud = command.Latitud,
                Longitud = command.Longitud,
                Speed = command.Speed,
                Gallons = command.Gallons,
                Temperature = command.Temperature,
                Date = command.Date
            };


            var gpsSaved = _writeableRepository.Create(gpsInfo);

            NotifyObservers(new GpsInfoAdded(gpsSaved.Id));
        }

        public event DomainEvent NotifyObservers;
    }

}
