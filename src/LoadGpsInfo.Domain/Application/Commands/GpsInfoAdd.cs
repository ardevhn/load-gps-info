﻿using System;
using LoadGpsInfo.Domain.Entities;

namespace LoadGpsInfo.Domain.Application.Commands
{
    public class GpsInfoAdd
    {
        public Guid id { get; protected set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float? Latitud { get; set; }
        public float? Longitud { get; set; }
        public float? Speed { get; set; }
        public float? Gallons { get; set; }
        public float? Temperature { get; set; }
        public DateTime? Date { get; set; }

        public GpsInfoAdd(Guid id, string name, string description, float? latitud, float? speed, float? gallons, float? temperature, DateTime? date)
        {
            this.id = id;
            this.Name = name;
            this.Description = description;
            this.Latitud = latitud;
            this.Longitud = Longitud;
            this.Speed = speed;
            this.Gallons = gallons;
            this.Temperature = temperature;
            this.Date = date;
        }
        
        protected GpsInfoAdd()
        {

        }
    }
}