namespace LoadGpsInfo.Domain
{
    public interface ITokenGenerator<out T>
    {
        T Generate();
    }
}