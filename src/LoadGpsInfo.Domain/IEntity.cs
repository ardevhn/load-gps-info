using System;

namespace LoadGpsInfo.Domain
{
    public interface IEntity
    {
        Guid Id { get; }
    }
}