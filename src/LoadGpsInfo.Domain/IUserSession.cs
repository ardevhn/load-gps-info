using System;

namespace LoadGpsInfo.Domain
{
    public interface IUserSession
    {
        Guid Id { get; }
    }
}