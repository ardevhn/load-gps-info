﻿using System;

namespace LoadGpsInfo.Domain.DomainEvents
{
    public class GpsInfoAdded
    {
        public Guid Id { get; protected set; }

        public GpsInfoAdded(Guid id)
        {
            this.Id = id;
        }

        protected GpsInfoAdded()
        {

        }
    }
}