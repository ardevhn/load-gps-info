using System;
using System.Configuration;

namespace LoadGpsInfo.Domain
{
    public class TokenExpirationProvider : ITokenExpirationProvider
    {
        public DateTime GetExpiration(DateTime now)
        {
            var expirationDays = Convert.ToInt32((string)(ConfigurationManager.AppSettings["PasswordExpirationDays"] ?? "10"));
            return now.AddDays(expirationDays);
        }
    }
}