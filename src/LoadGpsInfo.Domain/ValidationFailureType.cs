﻿namespace LoadGpsInfo.Domain
{
    public enum ValidationFailureType
    {
        Missing,
        DoesNotExist,
        Expired
    }
}