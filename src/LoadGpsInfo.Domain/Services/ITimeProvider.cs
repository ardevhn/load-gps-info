using System;

namespace LoadGpsInfo.Domain.Services
{
    public interface ITimeProvider
    {
        DateTime Now();
    }
}