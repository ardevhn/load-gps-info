using LoadGpsInfo.Domain.ValueObjects;

namespace LoadGpsInfo.Domain.Services
{
    public interface IPasswordEncryptor
    {
        EncryptedPassword Encrypt(string clearTextPassword);
    }
}