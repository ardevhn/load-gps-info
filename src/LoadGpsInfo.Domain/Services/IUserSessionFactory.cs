using LoadGpsInfo.Domain.Entities;

namespace LoadGpsInfo.Domain.Services
{
    public interface IUserSessionFactory
    {
        UserLoginSession Create(User executor);
    }
}