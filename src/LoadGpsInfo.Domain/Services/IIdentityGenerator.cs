namespace LoadGpsInfo.Domain.Services
{
    public interface IIdentityGenerator<out T>
    {
        T Generate();
    }
}